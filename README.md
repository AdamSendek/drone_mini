# Drone_Mini
Last update: 01-01-2019

## Description
An unmanned, bluetooth-controlled flying vehicle with an Atmega328p microcontroller and sensors: Accelerometer, Gyroscope, Magnetometer, Barometer and Thermometer.

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0
